/** @type {import('@jest/types').Config.InitialOptions} */
module.exports = {
  testMatch: ["**/*.test.ts"],
  testTimeout: 120000,
  maxWorkers: 1,
  globalSetup: 'detox/runners/jest/globalSetup',
  globalTeardown: 'detox/runners/jest/globalTeardown',
  reporters: ['detox/runners/jest/reporter'],
  testEnvironment: 'detox/runners/jest/testEnvironment',
  setupFilesAfterEnv: ["./setup.ts"],
  transform: {
    "\\.tsx?$": "ts-jest"
  },
  reporters: ["detox/runners/jest/reporter"],
  verbose: true,
};
