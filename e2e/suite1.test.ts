import { by, device, element, expect } from 'detox';

describe('Counter Tests', () => {

    beforeEach(async () => {
        await device.reloadReactNative();
    });

    it('Title Should be Visible', async () => {
        await expect(element(by.text('Contador'))).toBeVisible();
    });

    it('Button "+ 1" Should be Visible', async () => {
        await expect(element(by.text('+ 1'))).toBeVisible();
    });

    it('Button "- 1" Should be Visible', async () => {
        await expect(element(by.text('- 1'))).toBeVisible();
    });

    it('Validate increment 3 times', async () => {
        const button = element(by.text('+ 1'));
        for (var i = 0; i < 3; i++) {
            await button.tap()
        }
        await expect(element(by.text('Você clicou 3 vezes'))).toBeVisible();
    });

    it('Validate decrement 3 times', async () => {
        var button = element(by.text('+ 1'));
        for (var i = 0; i < 3; i++) {
            await button.tap()
        }
        await expect(element(by.text('Você clicou 3 vezes'))).toBeVisible();

        button = element(by.text('- 1'));
        for (var i = 0; i < 3; i++) {
            await button.tap()
        }
        await expect(element(by.text('Você clicou 0 vezes'))).toBeVisible();
    });
});
