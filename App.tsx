import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export default function App() {
  const [count, setCount] = useState(0);

  const incrementCounter = () => {
    setCount(count + 1);
  };

  const decrementCounter = () => {
    if (count > 0) setCount(count - 1);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Contador</Text>
      <Text style={styles.counter}>Você clicou {count} vezes</Text>
      <View style={styles.buttonsContainer}>
        <TouchableOpacity style={styles.buttonIncrement} onPress={incrementCounter}>
          <Text style={styles.buttonText}>+ 1</Text>
        </TouchableOpacity>
        <Text>   </Text>
        <TouchableOpacity style={styles.buttonDecrement} onPress={decrementCounter}>
          <Text style={styles.buttonText}>- 1</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  title: {
    fontSize: 32,
    fontWeight: 'bold',
    marginBottom: 16,
  },
  counter: {
    fontSize: 24,
    marginBottom: 32,
  },
  buttonText: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  buttonsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonIncrement: {
    backgroundColor: '#a7f27c',
    borderRadius: 8,
    padding: 16,
    paddingHorizontal: 24,
  },
  buttonDecrement: {
    backgroundColor: '#f27c7c',
    borderRadius: 8,
    padding: 16,
    paddingHorizontal: 24,
  },
});
